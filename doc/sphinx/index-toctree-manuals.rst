
Using and Programming
========================


.. toctree::
  :maxdepth: 2
  
  installation.rst
  
  introduction.rst
  
  tutorial.rst
  
  user.rst
  
  FEMxDEM.rst
  
  BayesianCalibration.rst

  prog.rst
  
  gitrepo.rst
  
  citing.rst
  
  fullPublications.rst
