import os
from yade import pack, export
from math import *
import numpy as np
from export import VTKExporter

# name of output folder
output_name = f'output_MPT'

# set periodic boundary conditions to be true
O.periodic = True

# dimensions of the domain
width = 0.8
height = 1
depth = 0.4

# bitmask for handling interactions between soil, cone, and the fluid
soil_mask = 0b0011
cone_mask = 0b0001
fluid_mask = 0b0010

# number of particles
num_particles = 2000
# porosity of the initial cloud
porosity = 0.8
# Cundall's damping (zero recommended)
damp = 0.0
# tolerance for the unbalanced force
tol = 1e-2

# control parameters for fluid injection
injection_rate = 0.1

# %% load the soil sample and the installed cone into the simulation
O.load(f"cone_installed/output_CPT_{num_particles}.yade")
periodic_checker.dead = True
vtkE.dead = True
vtkR.dead = True
O.run(flow.meshUpdateInterval, 1)

# %% set the output folder and enable the fluid engine
output_folder = f'output_MPT'
flow.dead = False
flow.bndCondIsPressure = [0, 0, 1, 1, 0, 0]
vtkE.dead = False
vtkE.iterPeriod = 25
vtkR.dead = False
vtkR.iterPeriod = 25

# %% impose a constant flux from the cone
flow.blockHook = "imposeFlux(injection_rate)"
# find the cone bodies and sort them by their y-coordinate
cone_bodies_info = [(b, b.state.pos[0], b.state.pos[1], b.state.pos[2]) for b in O.bodies if b.mask == 0b0001]
cone_bodies_info.sort(key=lambda x: x[2])
# find the clump bodies at the bottom of the cone
cone_coords = [Vector3(pos_x, pos_y, pos_z) for _, pos_x, pos_y, pos_z in cone_bodies_info if
               pos_y == cone_bodies_info[0][2]]


def imposeFlux(rate):
    # clear the previously imposed flux
    flow.clearImposedFlux()
    # assign a constant flux to the selected cone coordinates
    for coord in cone_coords:
        # TODO is this the correct way to impose a flux? The flux seems to get higher as the number of particles increases
        flow.imposeFlux(coord, -injection_rate)
    # block other part of the cone from the fluid engine
    for _, pos_x, pos_y, pos_z in cone_bodies_info:
        flow.blockCell(flow.getCell(pos_x, pos_y, pos_z), False)


# %% define the exporter for the VTK files
vtkExporter = export.VTKExporter(f'{output_name}/{num_particles}/CPT')


def export_data():
    vtkExporter.exportSpheres(
        what=dict(
            vel='b.state.vel'
        ))


# %% Run the simulation
O.run()
